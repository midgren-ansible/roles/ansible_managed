# Role ansible_managed

Install requirements needed for a system to be managed by Ansible.

This installs ssh and python3 with required packages. Actually SSH and
python are required to at all run Ansible so they must already be in
place for this role to be applied, however there are some python
packages that are valuable and hard to fit someplace else.

## Requirements

- ansible >= 2.7

## Role Variables

_None_

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ansible_managed
```
